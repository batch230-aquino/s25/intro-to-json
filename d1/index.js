console.log("Hello Mundo!");

//[SECTION] JSON Object
/*
    -JSON stands for Java Script Object Notation
    -is also used in another programming languages
    -Core JavaScript has a built in JSON objects that contatins methods for passing JSON objects\
    -JSON is used for serializing different data types into bytes
*/

// JSON Object
/*
    JSON also uses the "key/value pairs" just like the object properties in JavaScript
    -Key/Property names should be enclosed with double quotes

    syntax:
    {
        "propertyA" : "value",
        "propertyB" : "value"
    }
*/
/*
    {
        "city": "Quezon City",
        "province": "Metro Manila",
        "country" : "Phillippines"
    }

*/

//JSON Arrays
//Arrays in JSON are almost same as array in JavaScript
// Arrays in JSON Object
/*
    {
    "cities" : [
        "city" : "Quezon City",
        "province" : "Metro Manila",
        "country" : "Phillippines"
    
    },
    {
    "city" : "Manila City",
    "province": "Metro Manila",
    "country" : "Phillippines"
    },
    {
    "city" : "Makati City",
    "province": "Metro Manila",
    "country" : "Phillippines"
    }
]
*/

//[SECTION] JSON Methods
//The "JSON Object" contains methods for parsing and converting data in stringified JSON
//JSON data is sent or received in text-only(String) format

//Coverting Data Intro Stringified JSON

let batchesArr = [
    {
        BatachName: 230,
        schedule: "Part Time",
    },
    {
        BatachName: 240,
        schedule: "Full Time"
    }
];

console.log(batchesArr);
console.log("Result from Stringify method: ");
/*syntax: JSON.stringify(arrays/Objects) */
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify(
    {
        name: "John",
        age: 31,
        address:{
            city: "Manila",
            country: "Phillippines"
        }
    }
);

console.log(data);

// User Details
// let firstName = prompt("Enter your first name: ");
// let lastName = prompt("Enter your last name: ");
// let email = prompt("Enter your email: ");
// let password = prompt ("Enter your password");





// let details = JSON.stringify(
//     {
//         firstName : firstName,
//     lastName : lastName,
//     email : email,
//     password : password
//     }

// );

// console.log(details);

//Converting Stringified JSON to JavaScript Objects:

let batchesJSON = `
[
    {
        "batchName": 230,
        "schedule" : "Part Time"
    },
    {
        "batchName" : 240,
        "schedule" : "Full Time"
    }
]`;

console.log("batchesJSON content: ");
console.log(batchesJSON);

console.log("Result from parse method: ");

let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);

let stringifiedObject = `{
    "name" : "John",
    "age" : 31,
    "address": {
        "city" : "Manila",
        "country" : "Phillippines"
    }
}
`
console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));


